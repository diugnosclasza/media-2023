.. index::
   pair: Son; Sainte Soline
   ! Saint Soline

.. _son_saint_soline_2023_03_29:

=====================================================================================================
2023-03-29 🔊 **Violences policières : la LDH appelle à ne rien lâcher ! (enregistrement sonore)**
=====================================================================================================

- https://www.ldh-france.org/violences-policieres-la-ldh-appelle-a-ne-rien-lacher/
- https://www.change.org/p/retraites-stop-%C3%A0-l-escalade-r%C3%A9pressive

L’actualité des derniers jours ne fait que renforcer notre détermination
à lutter contre la violente répression en cours.

En fin de semaine dernière, des membres de plusieurs observatoires de la
LDH étaient présents à Sainte-Soline, dans les Deux-Sèvres, pour observer
le maintien de l’ordre dans le cadre des mobilisations contre les « mégabassines ».

En contradiction avec ce que prétend la préfète des Deux-Sèvres, les
équipes présentes ont observé une utilisation disproportionnée de la
force à l’encontre de l’ensemble des personnes présentes, et ce de manière
indiscriminée.

Plusieurs cas d’entraves par les forces de l’ordre à l’intervention des
secours ont été constatés.

Trois de nos avocats ont assisté à une conversation au cours de laquelle
le Samu a indiqué ne pouvoir intervenir pour secourir un blessé en état
d’urgence vitale, dès lors que le commandement avait donné l’ordre de
ne pas le faire.

Nous avons communiqué un extrait de cet enregistrement à la presse.

Nous en publions aujourd’hui l’intégralité sur `notre site Internet <https://www.ldh-france.org/violences-policieres-la-ldh-appelle-a-ne-rien-lacher/>`_, que
vous pouvez écouter et :download:`télécharger <son/LDH-sainte-soline.mp3>`


La situation est particulièrement grave et doit nous mobiliser toutes et tous,
c’est pourquoi nous vous invitons à `signer la pétition de la LDH <https://www.change.org/p/retraites-stop-%C3%A0-l-escalade-r%C3%A9pressive>`_ et
diffuser largement cet audio.
